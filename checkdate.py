# import datetime,timefhuman
# from dateparser.search import search_dates

from dateutil.parser import _timelex, parser
import datetime,holidays

a = "from 13 aug to 16 aug"
p = parser()
date=[]
info = p.info

def timetoken(token):
  try:
    float(token)
    return True
  except ValueError:
    pass
  return any(f(token) for f in (info.jump,info.weekday,info.month,info.hms,info.ampm,info.pertain,info.utczone,info.tzoffset))

def timesplit(input_string):
  batch = []
  for token in _timelex(input_string):
    if timetoken(token):
      if info.jump(token):
        continue
      batch.append(token)
    else:
      if batch:
        yield " ".join(batch)
        batch = []
  if batch:
    yield " ".join(batch)
  
india_holidays = holidays.India(years=2020)
for item in timesplit(a):
    if p.parse(item) < datetime.datetime.now():
        print("Entered date is in the past. Kindly enter a date in the near future")
        break
    if p.parse(item) in india_holidays or p.parse(item).weekday == 7:
        print("You cannot select holidays for leave")
        break
    else:
        #print("Found:",item)
        #print("Parsed:",p.parse(item))
        date.append(p.parse(item))

Start_Date = str(date[0])[0:11]
End_Date = str(date[1])[0:11]

#print(Start_Date)
#print(End_Date)