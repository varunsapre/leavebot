# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.events import SlotSet,Restarted,FollowupAction
from rasa_sdk.executor import CollectingDispatcher
import requests,json,datetime,holidays
from dateutil.parser import _timelex, parser
from checkdate import timesplit #importing the py file which extracts the two dates
from leaveapps import ShowLeaveApps

Start_Date = ""
End_Date = ""

class ActionHello(Action):
    def name(self) -> Text:
        return "action_hello"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        sender_id = (tracker.current_state())["sender_id"] 
        #sender_id = '1880981104275772'
        header = {"Authorization":sender_id}
        leave_url = 'http://204.141.208.30/cloudyhr/api/leave-management/leave-balance/'        
        u_det_url = 'http://204.141.208.30/cloudyhr/api/leave-management/details/'
        leave_req = requests.get(leave_url,headers=header)
        u_det_req = requests.get(u_det_url,headers=header)
        leave_result = leave_req.json()
        print(leave_result)
        u_det_result = u_det_req.json()
        print(u_det_result)
        emp_des = leave_result["Designation"]
        emp_name = u_det_result["Name"]
        pending_leaves = leave_result["RemainingCompoff"]
        leaves = int(leave_result["RemainingLeave"]) + pending_leaves
        
        if emp_des=="Admin Executive" or emp_des=="HR" or emp_des=="HR Admin":
            x = "Greetings HR Admin ! Welcome to HRMS support. Do you want to go through the leave applications or apply for a leave ?" 
            dispatcher.utter_message(x)
        else:
            x = "Greetings "+str(emp_name)+"!! Welcome to HRMS support. Your current leave balance is "+str(leaves)+". How Can I Assist?"
            dispatcher.utter_message(x)

class ActionCheckRemainingLeaves(Action):
    def name(self) -> Text:
        return "action_check_rem_leaves"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        sender_id = (tracker.current_state())["sender_id"]
        #sender_id = '1880981104275772'
        url = 'http://204.141.208.30/cloudyhr/api/leave-management/leave-balance/'
        req = requests.get(url,headers={'Authorization':sender_id})
        result = req.json()
        dispatcher.utter_message("Here are your details: ")
        dispatcher.utter_message(json_message=result)

        if result['RemainingLeave'] >= 0:
            pass
        else:
            dispatcher.utter_message("Sorry your leave balance is 0, you are not eligible for more leaves. Thank you")
            #dispatcher.utter_message("I am restarting now")
            return []
            
class ActionShowLeaveApplications(Action):
    def name(self) -> Text:
        return "action_show_leave_apps"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        sender_id = (tracker.current_state())["sender_id"]
        #sender_id = '1880981104275772'
        url = 'http://204.141.208.30/cloudyhr/api/leave-management/leave-requests/'
        val = {"StartDate":"01-nov-2019","EndDate":"30-dec-2019"}
        req = requests.post(url,json=val,headers={'Authorization': sender_id})
        result = req.json()
        # request_id = result["request_id"]
        # emp_name = result["FullName"]
        #print(result)
        dispatcher.utter_message("Here are all the applications pending for approval: ")
        dispatcher.utter_message(json_message=result)
        return [FollowupAction('utter_get_requestid')]


class ActionShowRequestDetails(Action):
    def name(self) -> Text:
         return "action_show_request_details"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        count = 0
        sender_id = (tracker.current_state())["sender_id"]
        #sender_id = '1880981104275772'
        requestid = int(tracker.get_slot('requestid'))
        url = 'http://204.141.208.30/cloudyhr/api/leave-management/leave-requests/'
        val = {"StartDate":"01-nov-2019","EndDate":"30-dec-2019"}
        req = requests.post(url,json=val,headers={'Authorization': sender_id})
        result = req.json()

        for i in result:
            count = count + 1 
            if i['RequestId']==requestid:
                dispatcher.utter_message(json_message=i)
                break

class ActionLeaveApplicationSent(Action):
    def name(self) -> Text:
         return "action_leave_application_sent"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        #sender_id = (tracker.current_state())["sender_id"]
        #sender_id = '1880981104275772'
        #date = tracker.get_slot('date')
        leavetype = str(tracker.get_slot('leavetype'))
        reason = tracker.get_slot('reason')
        print(reason)
        #requestid = tracker.get_slot('requestid')

        if "pel" in leavetype or "personal" in leavetype:
            leavetype = 1
        elif "mal" in leavetype or "maternal" in leavetype:
            leavetype = 2
        elif "pal" in leavetype or "paternal" in leavetype:
            leavetype = 3
        elif "sl" in leavetype or "sick" in leavetype:
            leavetype = 7
        elif "co" in leavetype or "comp" in leavetype:
            leavetype = 8
        elif "em" in leavetype or "emergency" in leavetype:
            leavetype = 1001
        elif "ot" in leavetype or "other" in leavetype:
            leavetype = 1003
        
        x = {"Start Date":Start_Date,"End Date":End_Date,"Leave Type":leavetype,"Reason":reason}
        dispatcher.utter_message("Here are your Application details : ")
        dispatcher.utter_message(json_message=x)
        dispatcher.utter_message("Great...!! Your leave Application is sent to the HR for approval")

class ActionAcceptDecision(Action):
    def name(self) -> Text:
         return "action_accept_decision"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

            decision = str(tracker.get_slot('decision'))
            if "approve" in decision:
                dispatcher.utter_message("OK, I will mark this request as APPROVED")
            if "reject" in decision:
                dispatcher.utter_message("OK, I will mark this request as REJECTED")


class ActionCheckDate(Action):
    def name(self) -> Text:
         return "action_check_date"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        date=[]
        global Start_Date,End_Date
        p=parser()
        india_holidays = holidays.India(years=2020)
        d = str(tracker.get_slot('date'))
        if d == None:
            dispatcher.utter_message("Please enter the date for your leaves!!")
            
        #$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$#        
        else:        
            for item in timesplit(d):
                if p.parse(item) < datetime.datetime.now():
                    print("Entered date is in the past. Kindly enter a date in the near future")
                    dispatcher.utter_message("Entered date is in the past. Kindly enter a date in the near future")
                    break
                if p.parse(item) in india_holidays or p.parse(item).isoweekday==6 or p.parse(item).isoweekday==7:
                    print("You cannot select holidays for leave")
                    dispatcher.utter_message("You cannot select holidays for leave")
                    break
                else:
                    date.append(p.parse(item))
            
        if len(date)==2:
            Start_Date = date[0].isoformat()
            End_Date = date[1].isoformat()
        else:
            Start_Date = date[0].isoformat()
            End_Date = None
        month = str(Start_Date)[5:7]
        print(Start_Date)
        print(End_Date)

        sender_id = '1880981104275772'
        header = {"Authorization":sender_id}
        url = 'https://ai.cloudyhr.com/api/leave-management/attendance/2019/'+month
        req = requests.get(url,headers=header)
        result = req.json()
            
        if len(date)==2:
            for i in result:
                if p.parse(i['Date']).isoformat() >= p.parse(Start_Date).isoformat() and p.parse(i['Date']).isoformat() <= p.parse(End_Date).isoformat():
                    if i['Status'] == "Present":
                        print("You are marked present on ",i['Date'])
                        dispatcher.utter_message("You are marked present on ",i['Date'])
                        dispatcher.utter_message("Select another date!!")
                        break
                    
        else:
            for i in result:
                if p.parse(i['Date']).isoformat() == p.parse(Start_Date).isoformat():
                    if i['Status'] == "Present":
                        print("You are marked present on ",i['Date'])
                        dispatcher.utter_message("You are marked present on ",text=i['Date'])
                        dispatcher.utter_message("Select another date!!")
                        break
        
        return [Start_Date,End_Date]
        #dispatcher.utter_message(json_message={"Start_Date":Start_Date,"End_Date":End_Date})

class ActionResetBot(Action):
    def name(self) -> Text:
        return "action_reset_bot"
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        return [Restarted()]

'''
class ActionSendRequestDecision(Action):
    def name(self) -> Text:
        return "action_send_request_decision"
    
    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
        #sender_id = (tracker.current_state())["sender_id"]
        header = {"Authorization":sender_id}
        #sender_id = '1880981104275772'
        requestid =str(tracker.get_slot('requestid'))
        decision = str(tracker.get_slot('decision'))
        #WHAT IS THE DIFFERENCE BETWEEN REQUEST_ID AND APPLICATION_ID
        if "approve" in decision:
            url = 'http://204.141.208.30/cloudyhr/api/leave-management/approve/'+application_id
            req = request.get(url,headers=header)
            result = req.json()
            print("APPLICATION ID "+application_id+" : APPROVED")
            dispatcher.utter_message(json_message=result)
        elif "reject" in decision:
            url = 'https://ai.cloudyhr.com/api/leave-management/reject'
            req = request.post(url,headers=header,json={"ApplicationId":application_id,"Remarks":"Rejected"})
            result = req.json()
            print("APPLICATION ID "+application_id+" : REJECTED")
            dispatcher.utter_message(json_message=result)
        ##get the value of result from ActionShowLeaveApplications and then extract the value of "requestid(application_id)" to approve/reject it.
'''             

# class ActionHelloHR(Action):
#     def name(self) -> Text:
#         return "action_hello_hr"
    
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        
#         mob_no = (tracker.current_state())["sender_id"]
#         db = MySQLdb.connect("localhost","varunsapre","password","emp_det")
#         cursor = db.cursor()
        
#         q1 = "select * from api_emp_det where emp_mob = " + mob_no + ";"
#         try:
#             x = cursor.execute(q1)
#             if x==0:
#                 print("Sorry, could not find you in our Database.")
#             else:
#                 result = cursor.fetchall()
#         except:
#             print("Can not execute q1.")
        
#         leaves = result[0][3]+result[0][4]+result[0][5]
#         p_leaves = result[0][6]
        
#         dispatcher.utter_message("Greetings HR Admin ! Welcome to HRMS support. Your current leave balance is "+str(leaves)+" and you have "+str(p_leaves)+" leave applications pending for approval.")
#         dispatcher.utter_message("Do you want to go through the leave applications or apply for a leave ?")
#         return[]

# class ActionBotGreet(Action):

#     def name(self) -> Text:
#          return "action_bot_greet"
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#         db=MySQLdb.connect("localhost","varunsapre","password","rasadb")
#         cursor=db.cursor()
#         name="Varun"
#         q1 = "select emp_name,tot_leaves from emp_det WHERE (emp_name = '%s');" % name
#         try:
#             cursor.execute(q1)
#             if cursor.execute(q1)==0:
#                 print("Sorry, could not find you in our Database.")
#             results = cursor.fetchall()
#             #print(results)
#             not all(results)
#             for row in results:
#                 name = row[0]
#                 leaves = row[1]
# 				# Now print fetched result
#                 details = [name,leaves]
#         except:
#             print ("Error 0: unable to fetch data")
	
#         db.close()
#         dispatcher.utter_message("Hello " + details[0] + " ,You have " + str(details[1]) + " leaves remaining.")
        
#         return []

# class ActionFillDetails(Action):
#     def name(self) -> Text:
#          return "action_fill_details"
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
#         db=MySQLdb.connect("localhost","varunsapre","password","rasadb")
#         cursor=db.cursor()
#         name="Dope"
#         mobno="1234567890"
#         d = str(tracker.get_slot('date'))
#         r = str(tracker.get_slot('reason'))
#         #-------------------------------------------------------------------------------------------------------#
#         q2 = "insert into emp_det2 values(%d,'%s','%s','%s','%s');" % (rid,name,mobno,d,r)
#         try:
#             cursor.execute(q2)
#             if cursor.execute(q2)==0:
#                 print("Sorry, could not find you in our Database.")
        
#         except:
#             print ("Error 1: unable to insert data")
#         #-------------------------------------------------------------------------------------------------------#        
#         q3 = "select * from emp_det2 order by %d desc limit 1;" % rid
#         #print(q3)
#         try:
#             cursor.execute(q3)
#             if cursor.execute(q3)==0:
#                 print("Sorry, could not find you in our Database.")
#             results = cursor.fetchall()
#             not all(results)
#             for row in results:
#                 name= row[1]
#                 mobno = row[2]
#                 leavedate = row[3]
#                 leavereason = row[4]
#                 details = [name,mobno,leavedate,leavereason]        
#         except:
#             print ("Error 2: unable to fetch data")
#         #-------------------------------------------------------------------------------------------------------#
#         db.close()
#         dispatcher.utter_message("Name: " + details[0] + "\n Mobile Number: " +details[1] + "\n Leave Date: " + details[2] + "\n Leave Reason: " + details[3])
#         return []



# class ActionHelloEmp(Action):
    
#     def name(self) -> Text:
#         return "action_hello_emp"
    
#     def run(self, dispatcher: CollectingDispatcher,
#             tracker: Tracker,
#             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        

#         mob_no = (tracker.current_state())["sender_id"]
#         db = MySQLdb.connect("localhost","varunsapre","password","emp_det")
#         cursor = db.cursor()

#         q1 = "select * from api_emp_det where emp_mob = " + mob_no + ";"
#         try:
#             x = cursor.execute(q1)
#             if x==0:
#                 print("Sorry, could not find you in our Database.")
#             else:
#                 result = cursor.fetchall()
#         except:
#             print("Can not execute q1.")
#         #print(result[0][0])
#         leaves = result[0][3]+result[0][4]+result[0][5]
#         p_leaves = result[0][6]
#         dispatcher.utter_message("Greetings "+result[0][1]+"!!! Welcome to HRMS support. Your current leave balance is "+str(leaves)+" Any you have "+str(p_leaves)+" leave applications pending for approval. How Can I Assist?")
#         return[]