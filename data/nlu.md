## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
- how are you
- what's up
- hi there
- whats up
- heya
- helllllo
- hey there bot
- hey bot
- hy
- hey botya
- Hello

## intent:goodbye
- bye
- goodbye
- see you around

## intent:bot_challenge
- are you a bot?
- are you a human?
- am I talking to a bot?
- am I talking to a human?

## intent:thanks
- that all for now
- thanks
- thank you
- that would be it for now
- thnx

## intent:leave
- is it possible for me to get a leave
- can i get a leave?
- when can i get a leave
- leave
- when will it be possible to get a leave
- can you help me get a leave
- can you help me in getting a leave
- i want a leave on 31 december
- i want a leave
- can i get leave
- i want leave
- want leave
- can i request for a leave
- can i get a leave
- i want to apply for a leave
- i wanted to apply for a leave

## intent:leavedate
- [two days after tomorow](date)
- [3 days from 29 dec](date)
- from [13 dec to 16 dec](date)
- [the last 2 days of this month](date)
- [28 dec to 31 dec](date)
- [18 dec](date) to [31 dec](date)
- [last two days of december](date)
- [31 december](date)
- [2 days from tomorow](date)
- [tomorow](date)
- [18 dec](date)
- [this monday](date).
- i want leave on [31 dec](date)
- i want leave on [31 december](date)
- [2 to 5 january](date)
- [28 to 31 dec](date)
- [13 to 25 january](date)
- [28 to 31 december](date)
- [two days from 31 december](date)
- [two days after 31 december](date)
- i want leave from [28 to 31 december](date)
- [28 dec to 5 january](date)
- [31 december to 5 january](date)
- [1 to 5 january](date)
- [29 december](date)
- [28 december](date)
- [5 days from 1 january](date)
- [31 dec to 5 jan](date)
- [1 dec to 5 dec](date)
- [1 jan to 5 jan](date)
- [1 to 5 jan](date)
- i want leave [from 5 jan to 10 jan](date)
- [3 to 8  feb](date)
- [1 to 5 dec](date)
- i want leave on [13 december](date)
- i want leave [from 5 to 8 jan](date)
- [2 to 5 jan](date)
- [3 to 6 jan](date)
- [5 to 10 jan](date)
- [3 to 5 jan](date)
- [3 march](date)
- [2 feb](date)
- [1 jan](date)
- [15 feb](date)
- [10 feb](date)
- [5 feb](date)
- [11 feb](date)
- [17 jan](date)
- [5 jan](date)
- [4 march](date)
- leave on [12 jan](date)
- i want leave from [1 to 5 jan](date) as i am very [sick](reason)
- [1 to 15 jan](date)
- [1 to 10 jan](date)
- [1 to 6 jan](date)
- [10 to 15 jan](date)
- [1 to 5 december](date)
- [29 march](date)
- [4 dec](date)
- i want leave on 5 jan
- [10 to 12 jan](date)
- [10 jan](date)
- [9 jan](date)
- [from 10 to 13 jan](date)
- [15 -20 jan](date)
- [15 jan](date)
- [7 to 10 jan](date)
- i want leave on [31 jan](date)
- [20 to 26 jan](date)
- [14 jan to 16 jan](date)
- [20 to 23 feb](date)
- [10 -12 jan](date)

## intent:leavebalance
- How many leave I have?
- how many leaves do i have
- do i have any leaves remaining
- how many leaves are left
- can you tell me how many leaves do i have
- remaining leaves

## intent:PEL
- [PEL](leavetype)
- [pel](leavetype)
- [personal](leavetype)
- [personal leave](leavetype)

## intent:MAL
- [MAL](leavetype)
- [mal](leavetype)
- [maternity](leavetype)
- [maternity leave](leavetype)

## intent:PAL
- [PAL](leavetype)
- [pal](leavetype)
- [paternity](leavetype)
- [paternity leave](leavetype)

## intent:SL
- [SL](leavetype)
- [sl](leavetype)
- [sick](leavetype)
- [sick leave](leavetype)

## intent:CO
- [CO](leavetype)
- [co](leavetype)
- [compensative](leavetype)
- [compensative off](leavetype)
- [compensative off leave](leavetype)

## intent:EM
- [EM](leavetype)
- [em](leavetype)
- [emergency](leavetype)
- [emergency leave](leavetype)

## intent:OT
- [OT](leavetype)
- [ot](leavetype)
- [other](leavetype)
- [other leave](leavetype)

## intent:reason
- [emergency](reason)
- [sick](reason)
- going to [native place](reason)
- [i am sick](reason)
- [i am going to my native place](reason)
- i have an [emergency](reason)
- i [dont feel good](reason)
- i am very [sick](reason)
- [sick](reason) leave
- [family](reason) [emergency](reason)
- i have to [go to my native place](reason)
- [native place](reason)
- [native](reason)
- [personal](reason)
- [oov](reason)

## intent:checkleaveapps
- [check leave applications]
- [i want to see leave applications]
- [i want to see the pending leave applications]
- [can i see the leave applications]
- [leave applications]

## intent:requestid
- [12](requestid)
- [81](requestid)
- [45](requestid)
- [61](requestid)
- [59](requestid)
- [52](requestid)
- [71](requestid)
- [77](requestid)
- [90](requestid)
- [23](requestid)
- [oov](requestid)

## intent:approve
- [approve](decision)
- [approve this](decision)
- [approve this application](decision)
- [approve this leave application](decision)
- [i want to approve this](decision)
- [i approve this leave application](decision)
- [i approve this leave](decision)
- [i approve this application](decision)

## intent:reject
- [reject](decision)
- [reject this](decision)
- [reject this application](decision)
- [reject this leave application](decision)
- [i want to reject this](decision)
- [i reject this leave application](decision)
- [i reject this leave](decision)
- [i reject this application](decision)

## intent:hrreason
- [too many leaves](hrreason)
- [you have taken too many leaves](hrreason)
- [not possible](hrreason)
- [For now, i have been instructed not to grant leaves to any employee](hrreason)
- [orders from my seniors](hrreason)
- [order from the ceo](hrreason)
- [oov](hrreason)

## intent:continue
- [continue]
- [resume]
- [see the remaining applications]

## intent:exit
- [exit]
- [leave]
- [bye]
